﻿using System;

namespace TestDrivenJavascript.Web.Domain
{
    public class Person
    {
        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }


        public string GetFullName()
        {
            if (string.IsNullOrEmpty(LastName))
                throw new MissingFieldException("LastName is empty");

            return string.Format("{0} {1} {2}", FirstName, MiddleName, LastName);
        }
    }
}