﻿
function Person(firstName, middleName, lastName, oldAge) {

    var _fName = firstName;
    var _lName = lastName;
    var _mName = middleName;
    var _age = oldAge;

    if (_lName === "") {
        throw "Lastname is undefined";
    }

    if (_fName === "") {
        throw "FirstName is undefined";
    }

    if (_mName === "") {
        throw "MiddleName is undefined";
    }

    if (_age === "") {
        throw "Age is undefined";
    }

    this.getFullName = function () {
        return _fName + " " + _mName + " " + _lName;
    };

    this.getAge = function () {
        return _age;
    };

    this.addAge = function (yearsToAdd) {
        _age = _age + yearsToAdd;
    };
}
