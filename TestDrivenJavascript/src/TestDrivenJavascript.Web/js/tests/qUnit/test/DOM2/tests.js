﻿/// <reference path="../../lib/jquery.js" />
/// <reference path="code.js" />

module('DOM Todoitem Tests', {
    setup: function () {
        $("body").append("<ul id='todoList'></ul>");
    },
    teardown: function () {
        $("body").remove("#todoList");
    }
});

test('CreateTodoItem creates 1 element', function() {
    DomTester.CreateTodoItem();
    strictEqual($(".todoitem").length, 1);
});
