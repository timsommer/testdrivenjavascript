﻿/// <reference path="code.js" />
/// <reference path="../../lib/jquery.js" />

module('Asynchronous Tests', {
    setup: function () {
        $("body").append("<div id='divke'>T4T Rocks!</div>");
    },
    teardown: function () {
        $("#divke").remove();
    }
});

test('asynchronous timing test', function () {
    //Stop the testrunner to wait for async tests to run. Call start() to continue.
    stop();

    setTimeout(function () {
        ok(true);

        //Start running tests again after the testrunner was stopped. See stop().
        start();
    }, 100);
});

test('asynchronous timing test 2', function () {
    //Optional argument to merge multiple stop() calls into one. Use with multiple corrsponding start() calls.
    stop(2);

    setTimeout(function () {
        ok(true);
        console.log('longer timout finished');
        start();
    }, 200);

    setTimeout(function () {
        ok(true);
        console.log('smaller timout finished');
        start();
    }, 100);
});

//Add an asynchronous test to run. The test must include a call to start().
asyncTest('better asynchronous timing test', function () {
    setTimeout(function () {
        ok(true);
        start();
    }, 100);
});

//Add an asynchronous test to run. The test must include a call to start().
asyncTest('UI asynchronous timing test', function () {
    DomTester.fadeOutDiv(500, function () {
        ok(!$("#divke").is(":visible"));
        start();
    });
});


