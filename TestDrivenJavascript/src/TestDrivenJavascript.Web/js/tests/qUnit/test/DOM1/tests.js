﻿/// <reference path="../../lib/jquery.js" />
/// <reference path="code.js" />

var divName = "divke";

module('DOM Tests', {
    setup: function () {
        $("body").append("<div id='" + divName + "'>T4T Rocks!</div>");
    },
    teardown: function () {
        $("#" + divName).remove();
    }
});

test('DOM Test innertext', function () {
    strictEqual(DomTester.ReadElement(divName), 'T4T Rocks!');
});

module('DOM Tests 2', {
    setup: function () {
        $("body").append("<div id='" + divName + "'>qUnit DOM Testing is awesom!</div>");
    },
    teardown: function () {
        $("#" + divName).remove();
    }
});

test('DOM test other innertext', function () {
    strictEqual(DomTester.ReadElement(divName), 'qUnit DOM Testing is awesom!');
});

test('DOM test innertext without DomTester', function () {
    strictEqual($("#" + divName).text(), 'qUnit DOM Testing is awesom!');
});