﻿/// <reference path="../../../jasmine/lib/jquery.js" />
/// <reference path="../../../../app/Person.js" />

module('Person');

test("Able to initialize", function () {
    var _p = new Person();
    ok(_p, "Person initialized");
});

test("Known members should be defined", function () {
    var _p = new Person();

    ok(_p, "Person initialized");
    equal(_p.firstName, undefined, "firstname is undefined");
    equal(_p.lastName, undefined, "lastName is undefined");
    equal(_p.middleName, undefined, "middleName is undefined");
    equal(_p.age, undefined, "age is undefined");
    ok(_p.getFullName, "getFullName is defined");
    ok(_p.getAge, "getAge is defined");
    ok(_p.addAge, "addAge is defined");
});

test("Should return the right name", function () {
    var _p = new Person("Tim", "is", "Cool", 26);
    equal(_p.getFullName(), "Tim is Cool", "'Tim is Cool', right name returned");
});

test("Should throw exception", function () {
    throws(
        function () {
            var _p = new Person("Tim", "s", "", 15);
            _p.getFullName();
        },
        "Exception raised"
    );
});

test("Age should be added", function () {
    var _p = new Person("Tim", "is", "Cool", 15);
    _p.addAge(5);

    equal(_p.getAge(), 20, "Age was 20");
});

module('Test module', {
    setup: function () {

    },
    teardown: function () {

    }
});

test('my second module test', function () {
    ok(true);
});