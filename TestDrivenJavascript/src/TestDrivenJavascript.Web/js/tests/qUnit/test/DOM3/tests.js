﻿/// <reference path="../../lib/jquery.js" />

module('DOM Todoitem Tests', {
    setup: function () {
        $("body").append('<div id="mytestdiv" class="testclass" data-somedata="3"><span>some</span> content</div>');
    },
    teardown: function () {
    }
    
});

test('DOM test', function () {
    strictEqual($("#mytestdiv").length, 1);
    strictEqual($("#mytestdiv").text(), 'some content');
    strictEqual($("#mytestdiv.testclass").length, 1);
    strictEqual($("#mytestdiv span").text(), 'some');
});