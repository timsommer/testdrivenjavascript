﻿/// <reference path="code.js" />
/// <reference path="jquery.js" />

describe("Calculator", function () {
    var _calc;
    var _outputId = "#calc-fixture";

    beforeEach(function () {
        $("body").append("<div id='template-wrapper'><div id='calc-fixture'>some content</div></div>");
        _calc = new Calculator($(_outputId));
    });

    afterEach(function () {
        $("#calc-fixture").remove();
    });

    it("should be able to add 1 plus 1", function () {
        _calc.add(1, 1);
        expect($(_outputId).text()).toBe('2');
    });

    it("should be able to divide 3 into 12", function () {
        _calc.divide(12, 3);
        expect($(_outputId).text()).toBe('4');
    });

});
