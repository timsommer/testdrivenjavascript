﻿
var myObj = {
    save: function() {
    },
    getQuantity: function() {
        return 5;
    }
};

/* Jasmine’s test doubles are called spies. 
 * A spy can stub any function and tracks calls to it and all arguments. 
 */

describe("Spies-SpyOn", function () {
    it('should spy on save', function () {
        var _spy = spyOn(myObj, 'save');
        myObj.save();
        expect(_spy).toHaveBeenCalled();
    });

    it('should spy on getQuantity', function () {
        var _spy = spyOn(myObj, 'getQuantity').andReturn(10);
        expect(myObj.getQuantity()).toEqual(10);
    });

    it('should spy on getQuantity fake', function () {
        var _spy = spyOn(myObj, 'getQuantity').andCallFake(function () {
            console.log('returning 20');
            return 20;
        });
        expect(myObj.getQuantity()).toEqual(20);
    });

    it('should spy on getQuantity callthru', function () {
        var _spy = spyOn(myObj, 'getQuantity').andCallThrough();
        expect(myObj.getQuantity()).toEqual(5);
        expect(_spy).toHaveBeenCalled();
    });

    it('should spy on getQuantity throw', function () {
        var _spy = spyOn(myObj, 'getQuantity').andThrow(new Error('problem'));
        var _qty;
        try {
            _qty = myObj.getQuantity();
        } catch (_ex) {
            _qty = 100;
        }
        expect(_qty).toEqual(100);
    });
});

