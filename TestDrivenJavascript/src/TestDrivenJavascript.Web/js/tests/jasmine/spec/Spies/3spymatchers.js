﻿
describe("SpyMatchers", function () {

    /* When there is not a function to spy on, jasmine.createSpy can create a “bare” spy. 
     * This spy acts as any other spy – tracking calls, arguments, etc. 
     * But there is no implementation behind it. 
     * Spies are JavaScript objects and can be used as such.
     */
    
    it('should verify arguments', function () {
        var _spy = jasmine.createSpy('mySpy');
        _spy(1);
        _spy(2);
        _spy(1, 1);
        expect(_spy).toHaveBeenCalledWith(1);
    });

    it('should verify arguments that werent called', function () {
        var _spy = jasmine.createSpy('mySpy');
        _spy(1);
        _spy(4, 1);
        expect(_spy).not.toHaveBeenCalledWith(4);
    });

    it('should work with metadata', function () {
        var _myObj = {
            method: function() {
            }
        };
        
        var _spy = spyOn(_myObj, "method");
        _myObj.method(1);
        _myObj.method(2);
        _myObj.method(3);
        expect(_spy.calls[0].args[0]).toEqual(1);
        expect(_spy.calls[0].object).toEqual(_myObj);
        expect(_spy.calls.length).toEqual(3);
        expect(_spy.callCount).toEqual(3);
        expect(_spy.mostRecentCall.args[0]).toEqual(3);
        expect(_spy.argsForCall[1][0]).toEqual(2);
    });

    it('should work with utility methods', function () {
        var _spy = jasmine.createSpy('a spy');
        expect(jasmine.isSpy(_spy)).toEqual(true);
        _spy();
        expect(_spy.callCount).toEqual(1);
        _spy.reset();
        expect(_spy.callCount).toEqual(0);
    });

});
