﻿
function callMyCallback(cb) {
    cb();
}

describe("SpiesCallback", function () {
    it("should spy on a callback", function () {

        var _spyCb = jasmine.createSpy('myspy');
        callMyCallback(_spyCb);
        expect(_spyCb).toHaveBeenCalled();
    });

});

