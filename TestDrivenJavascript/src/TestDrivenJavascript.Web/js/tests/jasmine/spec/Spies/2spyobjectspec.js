﻿
describe("Spies", function () {

    it("should create a spy object", function () {
        
        /* In order to create a mock with multiple spies, use jasmine.createSpyObj 
         * and pass an array of strings. 
         * It returns an object that has a property for each string that is a spy.*/
        var _spy = jasmine.createSpyObj('mySpy', ['getName', 'save']);
        _spy.getName.andReturn('bob');
        _spy.save.andCallFake(function () { console.log('save called'); });

        expect(_spy.getName()).toEqual('bob');
        _spy.save();
        expect(_spy.save).toHaveBeenCalled();
    });

});
