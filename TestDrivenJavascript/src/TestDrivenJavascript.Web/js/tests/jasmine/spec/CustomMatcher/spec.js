﻿/// <reference path="code.js" />

describe("Calculator", function () {
    var _calc;

    beforeEach(function () {
        _calc = new Calculator();

        /* Setup a custom matcher */
        this.addMatchers({
            toBeBetween: function (a, b) {
                return this.actual >= a && this.actual <= b;
            }
        });
    });

    it("should be able to add 1 plus 1", function () {
        expect(_calc.add(1, 1)).toBe(2);
    });

    it("should be able to add 100 plus 200", function () {
        expect(_calc.add(100, 200)).toBe(300);
    });

    it('should be able to divide 6 and 2', function () {
        expect(_calc.divide(6, 2)).toBe(3);
    });

    it('should be able to divide a rational number', function () {
        expect(_calc.divide(1, 3)).toBeBetween(0.3, 0.4);
    });
});
