﻿/// <reference path="../../../../app/Person.js" />

describe("Person", function () {
    it("Should be able to initialize", function () {
        var _p = new Person();
        expect(_p).toBeDefined();
    });

    it("Known members should be defined", function () {
        var _p = new Person();
        expect(_p).toBeDefined();
        expect(_p.firstName).not.toBeDefined();
        expect(_p.lastName).not.toBeDefined();
        expect(_p.middleName).not.toBeDefined();
        expect(_p.getFullName).toBeDefined();
        expect(_p.getAge).toBeDefined();
        expect(_p.addAge).toBeDefined();
        
    });

    it("Should return the right name", function () {
        var _p = new Person("Tim", "is", "Cool", 26);
        
        expect(_p.getFullName()).toMatch("Tim is Cool");

    });

    it("Should throw undefined exception", function () {
        var _testFn = function () {
            var _p = new Person("Tim", "", "", 15);
            _p.getFullName();
        };

        expect(_testFn).toThrow(new Error("Lastname is undefined"));

        /* That’s because we can’t call the function as the expect parameter; 
         * we need to hand it a function and let it call the function itself. 
         * Since we need to pass a parameter to that to function, we can do it this way.
         */
    });

    it("Should increment age", function () {
        var _p = new Person("Tim", "is", "Cool", 15);

        _p.addAge(5);
        
        expect(_p.getAge()).toMatch(20);
    });
});




