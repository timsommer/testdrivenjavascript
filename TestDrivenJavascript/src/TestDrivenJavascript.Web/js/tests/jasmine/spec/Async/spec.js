﻿/// <reference path="code.js" />
/// <reference path="../../lib/jasmine.async.min.js" />
/// <reference path="../../lib/jquery.js" />


describe("Calculator", function () {
    var _calc, _el;
    var _async = new AsyncSpec(this);
    
    beforeEach(function () {
        _el = $("<div>T4T Rocks!</div>");
        $("body").append(_el);
        _calc = new Calculator(_el);
    });

    afterEach(function () {
        _el.remove();
    });

    //As described in the jasmine documentation
    it('should work with a visual effect', function () {
        var _flag = false;
        var _callback = function() {
            _flag = true;
        };

        /* Specs are written by defining a set of blocks with calls to runs, 
         * which usually finish with an asynchronous call.
         */
        runs(function () {
            _calc.hideResult(_callback);
        });

        /* The waitsFor block takes a latch function, a failure message, and a timeout.*/

        /* The latch function polls until it returns true or the timeout expires, 
         * whichever comes first. If the timeout expires, the spec fails with the error message.
         */
        waitsFor(function () {
            return _flag;
        }, "flag to be true", 1100);

        /* Once the asynchronous conditions have been met, another runs block defines final test behavior. 
         * This is usually expectations based on state after the asynch call returns.
         */
        runs(function () {
            expect(_el.css("display")).toBe("none");
        });

    });





    //better way ?
    _async.it('should make the result invisible', function (done) {
        /* Callback method that has the assertion within its scope
         */
        var _callback = function () {
            expect(_el.css('display')).toBe("none");
            done();
        };

        /* The invokation with te callback method */
        _calc.hideResult(_callback);
    });
});