﻿using NUnit.Framework;
using TestDrivenJavascript.Web.Domain;

namespace TestDrivenJavascript.Tests.Tests
{
    [TestFixture]
    public class TestPerson
    {
        [Test]
        public void TestFullName()
        {

            var person = new Person();
            person.LastName = "Doe";
            person.MiddleName = "Roe";
            person.FirstName = "John";

            var actual = person.GetFullName();
            const string expected = "John Roe Doe";

            Assert.AreEqual(expected, actual, "The GetFullName returned a different Value");
        }
    }
}