﻿using NUnit.Framework;
using TestDrivenJavascript.Web.Domain;

namespace TestDrivenJavascript.Tests.Tests
{
    [TestFixture]
    public class TestSingleton
    {
        [Test]
        public void TestSingletonSingleInstance()
        {
            var singleton = Singleton.Instance;
            var singleton2 = Singleton.Instance;
            Assert.AreEqual(singleton, singleton2);
        }
    }
}
